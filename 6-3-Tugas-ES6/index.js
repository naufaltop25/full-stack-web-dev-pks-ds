// Soal 1
// Jawaban soal 1
const LuasKelilingPersegiPanjang = (panjang, lebar) => {
    return panjang * lebar;
}

// Soal 2
// Jawaban soal 2
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
          console.log(firstName + " " + lastName);
      }
    }
}

// Soal 3
// Jawaban soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject;

// Soal 4
// Jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];

// Soal 5
// Jawaban soal 5
const planet = "earth" 
const view = "glass" 

const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}`;


// test Code 
console.log(LuasKelilingPersegiPanjang(5,2));
newFunction("William", "Tech").fullName();
console.log(firstName, lastName, address, hobby);
console.log(combined);
console.log(before);

