<?php 

define('judul', '<h1>Tugas Pendalaman OOP</h1>');
echo judul;

abstract class Hewan {
    use Fight;

    protected $nama,
           $darah = 50,
           $jumlahKaki,
           $keahlian;

    public function atraksi($nama, $keahlian)
    {
        $this->nama = $nama;
        $this->keahlian = $keahlian;

        return "$nama sedang $keahlian";
    }
}

trait Fight {
    protected $attackPower, $defensePower;

    public function serang($hewan)
    {
        echo "$this->nama sedang menyerang $hewan->nama";
    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";
        $this->darah = $this->darah - $hewan->attackPower / $this->defensePower;
    }
}

class Elang extends Hewan 
{
    protected $jumlahKaki = 2, $keahlian = "terbang tinggi", $attackPower = 10, $defensePower = 5;  

    public function getInfoHewan($nama, $jenis = "Elang")
    {
        $this->nama = $nama;
        $this->jenis = $jenis;

        echo "Nama Hewan : $nama <br>";
        echo "Jenis Hewan : $jenis <br>";
        echo "Darah : $this->darah <br>";
        echo "Jumlah Kaki : $this->jumlahKaki <br>";
        echo "Keahlian : $this->keahlian <br>";
        echo "Attack Power : $this->attackPower <br>";
        echo "Defense Power : $this->defensePower <br>";
    }
}

class Harimau extends Hewan 
{
    protected $jumlahKaki = 4, $keahlian = "lari cepat", $attackPower = 7, $defensePower = 8;  

    public function getInfoHewan($nama, $jenis = "Harimau")
    {
        $this->nama = $nama;
        $this->jenis = $jenis;

        echo "Nama Hewan : $nama <br>";
        echo "Jenis Hewan : $jenis <br>";
        echo "Darah : $this->darah <br>";
        echo "Jumlah Kaki : $this->jumlahKaki <br>";
        echo "Keahlian : $this->keahlian <br>";
        echo "Attack Power : $this->attackPower <br>";
        echo "Defence Power : $this->defensePower <br>";
    }
}

$elang = new Elang();
$harimau = new Harimau();
echo "<br>";
echo $harimau->getInfoHewan("Harimau_2");
echo "<br>";
// echo $elang->serang();
echo $elang->getInfoHewan("Elang_1");
echo "<br>";
$elang->diserang($harimau);
echo "<br><br>";
echo $elang->getInfoHewan("Elang_1");
// echo $elang->atraksi("Elang", "Terbang Tinggi");
// echo $elang->serang("Elang", "nama");
