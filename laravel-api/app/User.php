<?php

namespace App;

use App\Roles;
use App\OtpCode;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $fillable = ['username', 'email', 'name', 'role_id', 'email_verified_at', 'password'];
    protected $keyType = 'string';
    public $incrementing = false;

    public function roles()
    {
      return $this->belongsTo('App\Roles');
    }

    public function otp_code()
    {
      return $this->hasOne('App\OtpCode', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            if(empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->role_id = Roles::where('name', 'testing')->first()->id;
        });

    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
