<?php

namespace App\Mail;

use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OtpCodeMail extends Mailable
{
    use Queueable, SerializesModels;
    public $otpCode, $newUserStatus;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpCode, $newUserStatus)
    {
        $this->otpCode = $otpCode;
        $this->newUserStatus = $newUserStatus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // if($newUserStatus)
        // {
        //     return $this->view('mails.name');
        // }
        return $this->view('mails.otpcode.mail')->subject('Full Stack Web Dev PKS DS');
    }
}
