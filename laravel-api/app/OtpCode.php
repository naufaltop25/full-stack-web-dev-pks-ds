<?php

namespace App;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    protected $fillable = ['otp', 'user_id', 'valid_until'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            if(empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
