<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        //get data from table role
        $roles = Roles::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar Data Roles',
            'data'    => $roles  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find roles by ID
        $roles = Roles::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Roles',
            'data'    => $roles 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'name',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Roles::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $role  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Roles $role)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find role by ID
        $role = Roles::findOrFail($role->id);

        if($role) {

            //update role
            $role->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $role  
            ], 200);

        }

        //data role not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find role by ID
        $role = Roles::findOrfail($id);

        if($role) {

            //delete role
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);

        }

        //data role not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}
