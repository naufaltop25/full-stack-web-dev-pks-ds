<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('post', 'PostController');
Route::apiResource('comment', 'CommentController');
Route::apiResource('role', 'RoleController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function() {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update-password');
    Route::post('login', 'LoginController')->name('auth.login');
});

// Route::get('/test', function() {
//     return "Masuk pak eko";
// })->middleware('auth:api');

// route post
// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store');
// Route::put('/post', 'PostController@update');
// Route::delete('/post', 'PostController@destroy');

// route comment
// Route::get('/comment', 'CommentController@index');
// Route::post('/comment', 'CommentController@store');
// Route::put('/comment', 'CommentController@update');
// Route::delete('/comment', 'CommentController@destroy');
